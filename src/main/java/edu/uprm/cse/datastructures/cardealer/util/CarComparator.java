package edu.uprm.cse.datastructures.cardealer.util;
import java.util.Comparator;
import edu.uprm.cse.datastructures.cardealer.model.Car;

public class CarComparator<E> implements Comparator<E> {

	@Override
	public int compare(E o1, E o2) {					//Adds up the strings and uses the function .compareTo
		Car car1 = (Car) o1;							//return -1 if o1 goes before and 1 if after
		Car car2 = (Car) o2;
		
		String string1 = car1.getCarBrand() + car1.getCarModel() + car1.getCarModelOption();
		String string2 = car2.getCarBrand() + car2.getCarModel() + car2.getCarModelOption();
		return string1.compareTo(string2);
	}
	

}
