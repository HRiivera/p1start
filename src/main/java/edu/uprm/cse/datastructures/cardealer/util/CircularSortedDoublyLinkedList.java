package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E>{

	public static class Node<E> {		//contains the element, reference to the next node, and reference to the previous node
		private E element;				//contains 1 header with null element
		private Node<E> next;
		private Node<E> previous;

		public Node(E element, Node<E> next, Node<E> previous) {
			super();
			this.element = element;
			this.next = next;
			this.previous = previous;
		}
		public Node() {
			super();
		}

		public E getElement() {
			return element;
		}
		public void setElement(E element) {
			this.element = element;
		}
		public Node<E> getNext() {
			return next;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}
		public Node<E> getPrevious(){
			return previous;
		}
		public void setPrevious(Node<E> previous) {
			this.previous = previous;
		}


	}

	private Node<E> header;
	private int currentSize;
	private Comparator<E> comp;

	public CircularSortedDoublyLinkedList(Comparator<E> comparator) {	//constructor. needs input comparator to be able to sort
		currentSize=0;
		header = new Node<E>(null, header, header);
		this.comp = comparator;
	}

	@Override
	public Iterator<E> iterator() {						//returns iterator
		return new IteratorL<E>(this.header);
	}

	@Override
	public boolean add(E obj) {									//if the list is empty,add it to the first position and link with header
		if(this.isEmpty()) {									//else search for the corresponding position and add it to that spot
			Node<E> newNode = new Node<E>(obj, header, header);
			this.header.setNext(newNode);
			this.header.setPrevious(newNode);
			currentSize++;
			return true;
		}
		Node<E> temp = this.header;

		while(temp.getNext()!=this.header) {
			temp= temp.getNext();
			if(comp.compare(obj, temp.getElement())<=+-1) {
				Node<E> newNode = new Node<E>(obj, temp, temp.getPrevious());
				temp.getPrevious().setNext(newNode);
				temp.setPrevious(newNode);
				this.currentSize++;
				return true;
			}
			if(temp.getNext()==this.header) {
				Node<E> newNode = new Node<E>(obj, header, header.getPrevious());
				header.getPrevious().setNext(newNode);
				header.setPrevious(newNode);
				this.currentSize++;
				return true;
			}
		}
		return false;
	}

	@Override
	public int size() {
		return this.currentSize;
	}

	@Override
	public boolean remove(E obj) {				//removes object in the first index
		int i = this.firstIndex(obj);			//returns false if not found
		if (i < 0) {
			return false;
		}else {
			this.remove(i);
			return true;
		}
	}

	@Override
	public boolean remove(int index) {						//removes object in given index
		if ((index < 0) || (index >= this.currentSize)){	//return false if not found
			throw new IndexOutOfBoundsException();	
		}
		Node<E> temp = this.header.getNext();
		if(!this.isEmpty()) {
		for(int i=0; i<index; i++) {
			temp = temp.getNext();
		}
		temp.getNext().setPrevious(temp.getPrevious());
		temp.getPrevious().setNext(temp.getNext());
		temp.setNext(null);
		temp.setPrevious(null);
		temp.setElement(null);
		this.currentSize--;
		return true;
		}
		return false;
	}
	
	@Override
	public int removeAll(E obj) {				//uses remove(obj) until it returns false
		int counter=0;							//returns amount of objects removed
		while(this.remove(obj)) {
			counter++;
		}
		return counter;
	}

	@Override
	public E first() {								//returns first element
		return (this.isEmpty() ? null : this.header.getNext().getElement());
	}

	@Override
	public E last() {							//returns last element
		return (this.isEmpty() ? null : this.header.getPrevious().getElement());
	}

	@Override
	public E get(int index) {					//returns the element in the input index
		if ((index < 0) || index >= this.currentSize) {
			throw new IndexOutOfBoundsException();
		}
		Node<E> temp = this.header.getNext();
		for(int i=0; i<index; i++) {
			temp = temp.getNext();
		}
		return temp.getElement();
	}

	@Override
	public void clear() {					//removes all elements
		while(!this.isEmpty()) {
			this.remove(0);
		}
	}

	@Override
	public boolean contains(E e) {

		return (firstIndex(e)>=0);
	}

	@Override
	public boolean isEmpty() {
		return this.currentSize==0;
	}

	@Override
	public int firstIndex(E e) {
		int i=0;
		for (Node<E> temp = this.header.getNext(); !temp.equals(header); 
				temp = temp.getNext(), ++i) {      //iterates until it finds e or reaches header
			if (temp.getElement().equals(e)) {
				return i;
			}

		}
		return -1; //not found
	}

	@Override
	public int lastIndex(E e) {
		int i=currentSize-1;
		for (Node<E> temp = this.header.getPrevious(); !temp.equals(header); 
				temp = temp.getPrevious(), --i) {      //iterates backwards until it finds e or reaches header
			if (temp.getElement().equals(e)) {
				return i;
			}

		}
		return -1; //not found
	}

}
