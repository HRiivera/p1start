package edu.uprm.cse.datastructures.cardealer.model;


import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.util.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

@Path("/cars")
public class CarService {

	private final static CircularSortedDoublyLinkedList<Car> cList = CarList.getInstance();
			
			
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {								//Returns the list in an array
		Car cArray[] = new Car[cList.size()];
		if(cList.isEmpty()) 
			return null;

		for(int i=0; i<cList.size(); i++) 
			cArray[i] = cList.get(i);
		
		return cArray;
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id){			//returns the car with the same id as the input
		for(int i=0; i<cList.size(); i++) {
			if(cList.get(i).getCarId()==id) {
				Response.status(Response.Status.OK).build();
				return cList.get(i);

			}
		}
		throw new NotFoundException();
	}

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)				//adds a new car to the list and returns response 201
	public Response addCar(Car car){
		cList.add(car);
		return Response.status(201).build();
	}


	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car) {						//searches for the car in the list with the same id and updates it to
		for(int i=0; i<cList.size(); i++) {						//its newer version. Returns response 200 if it was able to update the car
			if(cList.get(i).getCarId()==car.getCarId()) {		//and 404 if it failed to find a car to update
				cList.remove(i);
				cList.add(car);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}
	
	@DELETE
	@Path("{id}/delete")
	public Response deleteCar(@PathParam("id") long id) {		//deletes the car with the input id and returns response 200 if it was 
		for(int i=0; i<cList.size(); i++){						//able to delete and 404 if it could not find

			if(cList.get(i).getCarId() == id) {
				cList.remove(i);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}


}
