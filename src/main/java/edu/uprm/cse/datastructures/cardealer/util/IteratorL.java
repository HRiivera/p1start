package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Iterator;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList.Node;

public class IteratorL<E> implements Iterator<E> {

	Node<E> temp;

	public IteratorL(Node<E> head) {
		temp = head;
	}

	@Override
	public boolean hasNext() {
		if(temp.getNext() != null) {
			return true;
		}
		return false;
	}

	@Override
	public E next() {
		if(hasNext()) {
			temp = temp.getNext();
			return temp.getElement();
		}
		else {
			return null;			
		}

	}

}
