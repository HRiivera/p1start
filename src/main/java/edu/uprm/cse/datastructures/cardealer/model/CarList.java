package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

public class CarList {
	public static final CircularSortedDoublyLinkedList<Car> cList = new CircularSortedDoublyLinkedList<Car>(new CarComparator<Car>());
	
	  public static CircularSortedDoublyLinkedList<Car> getInstance(){			//returns cList list
	    return cList;
	  }
	  
	  public static void resetCars() {						//used in testers to reset the list
		  cList.clear();
	  }
}
